mssstest (3.0-8) unstable; urgency=medium

  * Team upload.
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.
  * Fix day-of-week for changelog entry 2.0-2.

 -- Nilesh Patra <npatra974@gmail.com>  Wed, 18 Nov 2020 17:57:32 +0530

mssstest (3.0-7) unstable; urgency=medium

  * Add fake watch file
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Thu, 01 Nov 2018 08:46:52 +0100

mssstest (3.0-6) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * debhelper 10
  * Standards-Version: 4.1.1

 -- Andreas Tille <tille@debian.org>  Tue, 28 Nov 2017 22:42:35 +0100

mssstest (3.0-5) unstable; urgency=medium

  * Team upload.
  * Fix building with GCC 6.
    - Add missing qualifier.
    Closes: #836930
  * Bump Standards-Version.
  * Fix typo in README.Debian.
  * Add full hardening.

 -- Sascha Steinbiss <satta@debian.org>  Sat, 17 Sep 2016 14:34:20 +0000

mssstest (3.0-4) unstable; urgency=low

  * debian/upstream/metadata: Added citation information
  * DEP5
  * debhelper 9
  * cme fix dpkg-control
  * DEP3 headers
  * propagate hardening options

 -- Andreas Tille <tille@debian.org>  Tue, 26 Jan 2016 11:39:01 +0100

mssstest (3.0-3) unstable; urgency=low

  * Finally removed debian/watch because upstream does not seem to
    release new versions
    Closes: #450107
  * debian/source/format: 3.0 (quilt)
  * debian/control:
    - Standards-Version: 3.9.2 (no changes needed)
    - Fixed Vcs fields
  * Debhelper 8 (control+compat)

 -- Andreas Tille <tille@debian.org>  Sat, 24 Dec 2011 09:43:01 +0100

mssstest (3.0-2) unstable; urgency=low

  * Deactivated debian/watch: Upstream does not use version
    numbering on downloadable tarball.
  * debian/control:
    - Standards-Version: 3.8.3 (added README.source)
    - Debhelper 7
    - Removed cdbs dependency
  * debian/rules: make use of dh

 -- Andreas Tille <tille@debian.org>  Tue, 06 Oct 2009 14:47:59 +0200

mssstest (3.0-1) unstable; urgency=low

  * New upstream version
  * New version can not be obtained via watch file from upstream
    but just out of the README file inside the archive.  Fixed
    watch file anyway to notice when upstream changed this policy
  * Group maintenance with permission of maintainer Steffen Möller
  * Moved manpage and wrapper script to debian directory
  * Added Homepage field
  * Standards-Version: 3.8.0 (no changes needed)
  * Vcs fields
  * XS-Autobuild: yes
  * No versioned debhelper dependency
  * Use quilt and cdbs
  * Fix gcc-4.3 problem
  * Use explicite path /usr/share/mssstest to the data files instead
    requiring the user to have these files in the local directory.
    This step was documented in README.Debian.  As a consequence
    mssstest.sh was removed because it is not needed any more
  * Patch makefile to get the name of resulting executable in lower
    cases and without .exe extension
  * debian/menu: Applications/Science/Medicine

 -- Andreas Tille <tille@debian.org>  Sun, 20 Jul 2008 21:51:26 +0200

mssstest (2.0-2) unstable; urgency=low

  * Reacting on bug report (Closes:Bug#292339)
    - No modifying upstream source.
    - Adding email indirectly proving upstream to agree
      with a distribution of MSSStest with Debian.

  * Moved some files from /usr/share/doc/mssstest
    to /usr/share/mssstest, binary now located in
    /usr/lib/mssstest.

  * Provided wrapper to start binary in /usr/lib/mssstest
    from /usr/bin/mssstest.

 -- Steffen Moeller <moeller@pzr.uni-rostock.de>  Thu, 05 May 2005 17:05:26 +0200

mssstest (2.0-1) unstable; urgency=low

  * Initial Release (Closes:Bug#283695).

 -- Steffen Moeller <moeller@pzr.uni-rostock.de>  Tue, 30 Nov 2004 16:32:26 +0100

mu = roxburgh
mi = neededroutines


MSSStest.exe: $(mi).o $(mu).o
	g++ -g -o MSSStest.exe $(mi).o $(mu).o -lm -lc


$(mi).o: $(mi).cpp $(mi).h
	g++ -c $(mi).cpp  -g -o $(mi).o

$(mu).o: $(mu).cpp
	g++ -c $(mu).cpp  -g -o $(mu).o


#include <iostream>
#include <cmath>
#include <fstream>
#include <iomanip>

using namespace std;

const double PI2 = 3.141593;

// Three consts needed for gammp() and gammq()
const int ITMAX = 100;
const double EPS = 3.0e-7;
const double FPMIN = 1.0e-30;

float gammln(float xx);

// For generating uniform random numbers (obtained from David Robelin)
 /* 
 Combined congruential and Tauseworthe generators from SuperDuper
 * package. Should work on machines with unsigned long of at least 32
 * bits. JC and JT must be initialized to values with 0 < JC < 2^32 and
 * 0 < JT < 2^32. JC must be odd.
 * References: Marsaglia, Ananthanarayanan & Paul, 1973,
 * Learmonth & Lewis, 1973, and Dudewicz, 1976)

tu dois avoir les lignes suivantes dans ton programme :

  unsigned long iseed=125645; // or some other integer
  sdrni(&iseed);

Il suffit que tu aies ecrit ces lignes avant d'appeler la fonction drand48().
 */

static unsigned long JC, JT;
static double Norm=4.656612873E-10;
double randunif();
void sdrni(unsigned long* i);
// End of bit for generating uniform random numbers

void sort2(unsigned long n, float arr[], int brr[]);

void rank(unsigned long len, float arr[], int ranks[]);

float rankties(unsigned long len, float arr[], float ranks[]);

float pchisq(int df, float x);
float gammp(float a, float x);
float gammq(float a, float x);
void gcf(float *gammcf, float a, float x, float *gln);
void gser(float *gamser, float a, float x, float *gln);

